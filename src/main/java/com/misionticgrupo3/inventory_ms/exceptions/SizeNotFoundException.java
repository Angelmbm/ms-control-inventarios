package com.misionticgrupo3.inventory_ms.exceptions;

public class SizeNotFoundException extends RuntimeException {
        public SizeNotFoundException(String message) {
            super(message);
        }
}