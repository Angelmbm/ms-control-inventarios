package com.misionticgrupo3.inventory_ms.controllers;

import com.misionticgrupo3.inventory_ms.exceptions.ProductNotFoundException;
import com.misionticgrupo3.inventory_ms.exceptions.InventoryNotFoundException;
import com.misionticgrupo3.inventory_ms.exceptions.SizeNotFoundException;
import com.misionticgrupo3.inventory_ms.models.Product;
import com.misionticgrupo3.inventory_ms.models.Delivery;
import com.misionticgrupo3.inventory_ms.repositories.ProductRepository;
import com.misionticgrupo3.inventory_ms.repositories.DeliveryRepository;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class DeliveryController {
    private final ProductRepository productRepository;
    private final DeliveryRepository deliveryRepository;

    public DeliveryController(ProductRepository productRepository, DeliveryRepository deliveryRepository) {
        this.productRepository = productRepository;
        this.deliveryRepository = deliveryRepository;
    }

    /*Despacho inventarios*/
    @PostMapping("/orders")
    Delivery newDelivery(@RequestBody Delivery delivery){
        Product deliveryOrigin = productRepository.findById(delivery.getOrderOrigin()).orElse(null);
        Product deliveryDestinity= productRepository.findById(delivery.getOrder()).orElse(null);

        if (deliveryOrigin == null)
            throw new InventoryNotFoundException("No se encontro un producto con el nombre: " + delivery.getOrderOrigin());

        if(deliveryOrigin.getInventory() < delivery.getQuantity())
            throw new InventoryNotFoundException("producto Insuficiente");

        deliveryOrigin.setInventory (deliveryOrigin.getInventory() - delivery.getQuantity());
        deliveryOrigin.setLastChange(new Date());
        productRepository.save(deliveryOrigin);

        deliveryDestinity.setInventory(deliveryDestinity.getInventory() + delivery.getQuantity());
        deliveryDestinity.setPrice(deliveryDestinity.getInventory() * deliveryOrigin.getPrice());
        deliveryDestinity.setLastChange(new Date());
        productRepository.save(deliveryDestinity);

        delivery.setValue(delivery.getQuantity() * deliveryOrigin.getPrice());
        delivery.setDate(new Date());
        return deliveryRepository.save(delivery);
    }

    @GetMapping("/orders/{name}")
    List<Delivery> productOrder(@PathVariable String name){
        Product orderProduct = productRepository.findById(name).orElse(null);
        if (orderProduct == null)
            throw new ProductNotFoundException("No se encontro una orden para: " + name);

        List<Delivery> ordersOrigin = deliveryRepository.findByOrderOrigin(name);
        List<Delivery> ordersDestinity = deliveryRepository.findByOrder(name);
        List<Delivery> orders = Stream.concat(ordersOrigin.stream(),
                ordersDestinity.stream()).collect(Collectors.toList());
        return orders;
    }
}