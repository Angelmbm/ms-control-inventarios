package com.misionticgrupo3.inventory_ms.controllers;

import com.misionticgrupo3.inventory_ms.exceptions.ProductNotFoundException;
import com.misionticgrupo3.inventory_ms.exceptions.InventoryNotFoundException;

import com.misionticgrupo3.inventory_ms.models.Product;
import com.misionticgrupo3.inventory_ms.repositories.ProductRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /*Crear producto*/
    @PostMapping("/products")
    Product newProduct(@RequestBody Product product){
        return productRepository.save(product);
    }

    /*Consultar productos*/
    @GetMapping("/products")
    List<Product> getAllProducts(){
        return productRepository.findAll();
    }
    /*Consultar productos X Id*/
    @GetMapping("/products/id/{id}")
    Product idProduct(@PathVariable String id){
        return productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException("No se encontro el producto con id: " + id));
    }
    /*Consultar productos X usuario*/
    @GetMapping("/products/target/{target}")
    List<Product> targetProducts(@PathVariable String target){
        return productRepository.findByTarget(target);
    }
    /*Consultar productos X categoria*/
    @GetMapping("/products/category/{category}")
    List<Product> categoryProducts(@PathVariable String category){
        return productRepository.findByCategory(category);
    }
    /*Consultar productos X nombre*/
    @GetMapping("/products/name/{name}")
    List<Product> nameProducts(@PathVariable String name, Integer inventory){
        if(productRepository.findByInventory(inventory) == null)
            throw new InventoryNotFoundException("No se encontro inventario del producto: " + name);
        return productRepository.findByName(name);
    }
    /*Consultar productos X talla*/
    @GetMapping("/products/size/{size}")
    List<Product> sizeProducts(@PathVariable Integer size, Integer inventory){
        if(productRepository.findByInventory(inventory) == null)
            throw new InventoryNotFoundException("No se encontro inventario de la talla: " + size);
        return productRepository.findBySize(size);
    }
    /*borrar productos X id*/
    @DeleteMapping("/products/delete/{id}")
    Product deleteroduct(@PathVariable String id) {
        productRepository.deleteById(id);
        throw new ProductNotFoundException("Http_Status_400.NOT_FOUND");
    }
    /*actualizar producto*/
    @PutMapping("/products/update/{id}")
    Product updateProduct(@RequestBody Product product){
        return productRepository.save(product);
    }
}