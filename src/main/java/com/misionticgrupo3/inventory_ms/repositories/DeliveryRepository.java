package com.misionticgrupo3.inventory_ms.repositories;

import com.misionticgrupo3.inventory_ms.models.Delivery;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface DeliveryRepository extends MongoRepository<Delivery, String> {
    List<Delivery> findByOrderOrigin (String orderOrigin);
    List<Delivery> findByOrder (String order);
}