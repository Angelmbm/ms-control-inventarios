package com.misionticgrupo3.inventory_ms.repositories;

import com.misionticgrupo3.inventory_ms.models.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface ProductRepository extends MongoRepository<Product, String> {
    List<Product> findByName (String name);
    List<Product> findByTarget (String target);
    List<Product> findByCategory (String category);
    List<Product> findBySize (Integer size);
    List<Product> findByInventory (Integer inventory);
}