package com.misionticgrupo3.inventory_ms.models;

import org.springframework.data.annotation.Id;
import java.util.Date;

public class Delivery {
    @Id
    private String id;
    private String orderOrigin;
    private String order;
    private Integer quantity;
    private Integer value;
    private Date date;

    public Delivery(String id, String orderOrigin, String order, Integer quantity, Integer value, Date date) {
        this.id = id;
        this.orderOrigin = orderOrigin;
        this.order = order;
        this.quantity = quantity;
        this.value = value;
        this.date = date;
    }
    public String getId() {
        return id;
    }
    public String getOrderOrigin() {
        return orderOrigin;
    }
    public void setOrderOrigin(String orderOrigin) {
        this.orderOrigin = orderOrigin;
    }
    public String getOrder() {
        return order;
    }
    public void setOrder(String order) {
        this.order = order;
    }
    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public Integer getValue() {
        return value;
    }
    public void setValue(Integer value) {
        this.value = value;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
}