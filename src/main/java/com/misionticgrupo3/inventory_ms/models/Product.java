package com.misionticgrupo3.inventory_ms.models;

import org.springframework.data.annotation.Id;
import java.util.Date;

public class Product {
    @Id
    private String id;
    private String name;
    private String target;
    private String category;
    private Integer size;
    private Integer price;
    private String image;
    private Integer inventory;
    private Date lastChange;

    public Product(String id, String name, String target, String category, Integer size, Integer price,
                   String image, Integer inventory, Date lastChange) {
        this.id = id;
        this.name = name;
        this.target = target;
        this.category = category;
        this.size = size;
        this.price = price;
        this.image = image;
        this.inventory = inventory;
        this.lastChange = lastChange;
    }
    public String getId() {
        return id;
    }
    public String getName() {

        return name;
    }
    public void setName(String name) {

        this.name = name;
    }
    public String getTarget() {

        return target;
    }
    public void setTarget(String target) {

        this.target = target;
    }
    public String getCategory() {

        return category;
    }
    public void setCategory(String category) {

        this.category = category;
    }
    public Integer getSize() {

        return size;
    }
    public void setSize(Integer size) {

        this.size = size;
    }
    public Integer getPrice() {

        return price;
    }
    public void setPrice(Integer price) {

        this.price = price;
    }
    public String getImage() {

        return image;
    }
    public void setImage(String image) {

        this.image = image;
    }
    public Integer getInventory() {

        return inventory;
    }
    public void setInventory(Integer inventory) {

        this.inventory = inventory;
    }
    public Date getLastChange() {

        return lastChange;
    }
    public void setLastChange(Date lastChange) {

        this.lastChange = lastChange;
    }
}